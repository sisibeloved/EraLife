import sys


def default(sub, obj, act, ia):
    global a
    p = a.data['km'].get(sub, obj)
    if '囚禁者' in obj.think(sub)['关系']:
        a.t('“快放{obj_自称}离开这里！”'.format(**p), True)
    elif act == '挥砍':
        a.t('“可恶！竟然敢用这木剑砍我！”'.format(**p), True)
    elif act == '打招呼':
        a.t('“我跟你很要好吗？来战！”'.format(**p), True)
    elif act == '聊天':
        a.t('“不要说话，来战！”'.format(**p), True)
    else:
        a.t('“切！”'.format(**p), True)
    a.t()
    a.back()


a = sys.argv[0]
a.data['kojo']['default'] = default
