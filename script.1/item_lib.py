# -*- coding: UTF-8 -*-
import os
import glob
import importlib
import core.game as game
import script.base_lib as lib
import script.era_lib as era


def load_pkg(pkg):
    for item in pkg.get_item_list():
        game.data['项目库'].append(item)


def load():  # 从模块列表分离出各个item
    for each in glob.glob('script\package\*.py'):
        text = 'script.package.'+os.path.basename(each).split('.')[0]
        pkg = importlib.import_module(text)
        load_pkg(pkg)


def find_item_by_name(index):
    for each in game.data['DLC']:
        if each.reg['index'] == index:
            return each
    return False
