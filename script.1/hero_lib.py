# -*- coding: UTF-8 -*-
import math
import random
import hashlib
import core.game as game
import script.base_lib as lib
import script.era_lib as era


def init():
    pass



def get_hero(hash):
    for each in game.data['人物库']:
        if each['hash'] == hash:
            return each
    return False


def get_normal(min, max):  # 180,分散度
    s = int(min)
    for each in range(int(min), int(max)):
        s += random.random()
    return int(s)


def get_enemy(level):
    enemy = get_init_hero()
    enemy['姓名'] = get_new_name('外文', 'female')
    enemy['性别'] = random.choice(['男性', '女性', '扶她'])
    enemy['等级'] = level
    enemy['状态']['存款'] = int(random.random() * level * 100)
    enemy['特征']['种族'].append(random.choice(era.get_item('种族'))['名称'])
    for each in range(0, level - 1):
        enemy['属性'][random.choice(['力量', '敏捷', '智力'])] += 1
    enemy['属性']['体力上限'] = 80 + level*20 + enemy['属性']['力量']*20
    enemy['属性']['耐力上限'] = 80 + level*20 + enemy['属性']['力量']*20
    enemy['属性']['精力上限'] = 80 + level*20 + enemy['属性']['智力']*20
    enemy['状态']['体力'] = enemy['属性']['体力上限']
    enemy['状态']['耐力'] = enemy['属性']['耐力上限']
    enemy['状态']['精力'] = enemy['属性']['精力上限']
    enemy['属性']['经验值上限'] = 80 + level * 20
    game.data['人物库'].append(enemy)
    return enemy


def get_height(age, gender):
    if gender == 'male'or gender == '男性':
        pass
    elif gender == 'female'or gender == '女性':
        pass
    else:
        pass


def get_hash():
    m = hashlib.md5()
    m.update(str(random.random()).encode("utf-8"))
    return m.hexdigest().upper()


def get_new_name(language, gender='female'):
    if language == '中文':
        name = random.choice(game.data['姓名库']['中文']['姓'])
        if gender == 'male':
            name += random.choice(game.data['姓名库']['中文']['男名'])
        elif gender == 'female':
            name += random.choice(game.data['姓名库']['中文']['女名'])
    elif language == '外文':
        if gender == 'male':
            name = random.choice(game.data['姓名库']['外文']['男名'])
        elif gender == 'female':
            name = random.choice(game.data['姓名库']['外文']['女名'])
    return name


def get_init_hero():
    new_hero = {
        '姓名': '',
        '身份': '',  # '主角'/''
        '称呼': {
            '系统称呼': '',
            '自称': '',  # 我/在下/本大小姐/本人/[姓名]
            '尊称': '',  # 他人对自己的尊称
            '蔑称': '',  # 他人对自己的蔑称
        },
        '性别': '',  # '男性'/'女性'/'扶她'/''
        '等级': 1,
        '状态': {
            '体力': 100,  # 0代表死亡
            '耐力': 100,  # 0代表脱力
            '精力': 100,  # 0代表昏厥
            '存款': 0,
            '经验值': 0,
        },
        '属性': {
            '身高': 160,
            '体重': 50,
            '胸围': 50,
            '腰围': 50,
            '臀围': 50,
            '力量': 0,
            '敏捷': 0,
            '智力': 0,
            '体力上限': 100,  # 80 + (等级-1)*20
            '耐力上限': 100,  # 80 + (等级-1)*20
            '精力上限': 100,  # 80 + (等级-1)*20
            '经验值上限': 100,  # 80 + (等级-1)*20
        },
        '特征': {
            '体型': '',
            '职业': [],
            '种族': [],
        },
        '经验': {},
        '物品': [],
        '装备': {
            '头部': '',
            '面部': '',
            '颈部': '',
            '上身': '',
            '左手': '',
            '右手': '',
            '双手': '',
            '手指': [],
            '下身': '',
            '脚部': '',
        },
        '服装': {
            '帽子': '',
            '面具': '',
            '项链': '',
            '内衣': '',
            '外衣': '',
            '手镯': '',
            '戒指': '',
            '手套': '',
            '内裤': '',
            '裤子': '',
            '袜子': '',
            '鞋子': '',
        },
        '关系': {  # Hash
            '父亲': '',
            '母亲': '',
            '子女': [],
            '恋人': [],
            '爱人': [],
        },
        '宝珠': [],
        '能力': [],  # 顺从 欲望 技巧 感觉 S属性 （有个位数等级的）
        '刻印': [],  # 苦痛 快乐 屈服 反发 淫纹
        '性格': [],
        '体质': [],  # 先天的、自然性、生物性的的Tag
        '技能': [],
        '魔法': [],
        '后天': [],  # 后天的、社会性的的Tag
        'hash': get_hash(),
    }
    # for each in game.data['项目库']:
    #     if each['']
    return new_hero


def get_default_hero():
    man = get_init_hero()
    man['姓名'] = '你'
    man['性别'] = '男性'
    man['身份'] = '主角'
    return man
