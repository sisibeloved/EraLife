# -*- coding: UTF-8 -*-
import core.game as game
import script.hero_lib as hero
import script.gui_lib as gui
import script.mainflow as mf


def get_item_list():
    item_list = []

    item = {
        'type': '排序',
        'parent': '',
        'name': 'create_hero_method',
        '名称': '角色创建方式',
        '排序': [
            'default'
        ],
    }
    item_list.append(item)

    def default_click():
        h = hero.get_default_hero()
        game.data['人物库'].append(h)
        gui.clear()
        gui.goto(mf.into_main)
    item = {
        'type': '角色创建方式',
        'parent': '',
        'name': 'default',
        '名称': '使用游戏默认初始角色',
        '点击': default_click,
    }
    item_list.append(item)

    return item_list
