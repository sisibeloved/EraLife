# -*- coding: UTF-8 -*-
import core.game as game
import script.base_lib as lib
import script.era_lib as era

reg = {
    'enabled': True,
    'name': 'sword',
    '名称': '剑'
}


def get_item_list():
    item_list = []

    def 可行(sub, obj):
        return True

    def 释放(sub, obj):
        t = '{}向{}一记轻拳，造成了10点伤害。'
        t = t.format(era.get_hero_by_hash(sub)['姓名'],
                     era.get_hero_by_hash(obj)['姓名'])
        game.pl(t)
        era.get_hero_by_hash(obj)['状态']['体力'] -= 10
    item = {
        'type': '动作',
        'parent': '战斗',
        'name': 'light_hit',
        '名称': '轻拳',
        '唯一': True,
        'func': None,
        '攻击力': 5,
        '价格': 100,
        '部位': ['左手', '右手'],
        '可行': 可行,
        '释放': 释放,
    }
    item_list.append(item)

    return item_list
