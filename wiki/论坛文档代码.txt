[align=center][attachimg]676[/attachimg]


[size=4][b]测试版更新/讨论帖[/b][/size][/align]
[size=3]自从 EraLife 在 [url=https://tieba.baidu.com/p/5913297784]贴吧[/url] 发布之后，受到了大家的广泛关注和支持，非常感谢。但现在贴吧环境较为不好，遂决定将后续更新及讨论内容转移至深渊，原帖将不再进行更新版发布。欢迎提出意见或建议，但请客观看待个人开发进度缓慢的情况。


[quote]
作者的闲言碎语：
　　大家好！又有段时间没见了！不知道大家又想我了吗(。・∀・)ノ
　　最近我在干什么呢？我在更新引擎呀！！！
　　Era.js游戏引擎承蒙大家厚爱，已经有不少同学关注并开始使用了，
　　而现在可以公布的情报是：【本次引擎新增】[color=Magenta]响应式界面[/color]+[color=DimGray][i]暗黑模式[/i][/color]！
[align=center]
[attachimg]4906[/attachimg]
[size=1]暗黑模式[/size]
[attachimg]4910[/attachimg]
[size=1]响应式界面（移动端友好）[/size]
[/align]
　　这次更新完全是基于自研的主题样式，强调了移动端友好与高自由度，可以让开发者随心所欲地魔改界面啦！另外以前版本的界面虽然让人耳目一新，但毕竟是套用的现成的主题样式，一套用下来有些略微不适合于文字量偏大的显示需求（按钮和文字对不齐啦！排版不好排啦！），于是我手撸了一套样式，专注于文字游戏100年。现在看来默认外观下效果还不错？欢迎大家提出意见和建议哈！
[align=right]
您永远热情的：Miswanting
2020年03月22日
[/align]
[/quote]
[align=center][size=4][b]游戏介绍[/b][/size][/align]
EraLife 是基于 Era.js 文字游戏引擎 的第一款作品，和 Era.js 引擎 并行开发，现已分别发布。


[align=center][size=4][b]游戏背景介绍[/b][/size]
[size=1][color=gray]（以下描述均为最终状态）[/color][/size][/align]
热血的你，现在正站在幻想大陆的正中央！开局一双手，成就自己在新世界中的霸业吧！
[b]游戏核心[/b]：养成，冒险，奇幻；
[b]游戏要素[/b]：拟人魔物，战斗，ＴＪ，技能树，魔法树，多v多，装备，合成，物品品质，职业，任务，市场，地图，房屋，成就，履历；
上百种魔物和特殊人物，数十个特色地图等你来探索。
多样的人物互动机制，还原动漫常见剧情。


[align=center][size=4][b]游戏特色[/b][/size]
[color=gray][size=1]（以下描述均为最终状态）[/size][/color][/align]
[b]【新引擎，老玩法】[/b]
[list]
[*]基于全新引擎打造，面向玩家与游戏开发者进行优化，给游玩和二次开发带来不一样的有趣体验；
[*]作为 Era.js 文字游戏引擎的示例程序，代码质量较高，注释覆盖率高，易于学习；
[*]自带游戏编辑器，不接触底层代码，也能任意创建/修改/删除游戏内容；
[*]游戏操作逻辑与 Emuera 引擎同步，上手0难度；
[*]游戏界面使用 Html5 技术，灵活且美观，远离排版烦恼；
[/list]

[b]【高自由度】[/b]
[list]
[*]硬核文字冒险游戏，面向游戏大后期拓宽玩法；
[*]含多周目要素，周目间继承逻辑丰富；
[*]不同存档间可交换数据；
[*]游戏脚本为 Python3，易学习，功能强大，自由度高；
[/list]

[b]【机制丰富】[/b]
[list]
[*]目指无限流，尽可能涵盖Era类游戏的主要元素；
[*]独特世界观，不依附于现有任何作品。
[/list]


[align=center][size=4][b]当前进度[/b][/size]
[color=gray][size=1]（更新时间：190707）[/size][/color]
　游戏界面：⬛⬛⬜⬜⬜⬜⬜⬜⬜⬜ 20% [color=red]↓[/color]
　游戏逻辑：⬛⬜⬜⬜⬜⬜⬜⬜⬜⬜ 10% [color=green]↑[/color]
　游戏数据：⬛⬜⬜⬜⬜⬜⬜⬜⬜⬜ 10% [color=green]↑[/color]
　游戏事件：⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜ 00% [color=black]0[/color]
游戏编辑器：⬛⬛⬜⬜⬜⬜⬜⬜⬜⬜ 20% [color=green]↑[/color]
　游戏引擎：⬛⬛⬛⬛⬜⬜⬜⬜⬜⬜ 40% [color=green]↑[/color][/align]

[align=center]【当前进度详情（按时间从旧到新）】
[size=1]（[color=orange]刚完成[/color]　[color=purple]已暂停[/color]　[color=red]阻塞中[/color]　[color=green]进行中[/color]　[color=blue]预研中[/color]　[color=gray]探索中[/color]）[/size][/align]
[list]
[*][color=orange]【游戏编辑器】将游戏编辑器开发完毕并投入使用。（已具有不完整的游戏数据的查看/新增/修改/删除功能）[/color]
[*][color=orange]【游戏逻辑】重构物品系统，现在每个物品都是唯一的。（已完成！）[/color]
[*][color=purple]【游戏数据】开发游戏内百科全书并导入并应用魔物娘百科中的全部设定（百科全书模块已实装，导入设定已完成50%）[/color]
[*][color=green]【游戏逻辑】修复已知 BUG[/color]
[*][color=green]【游戏引擎】重写并升级至 v0.2[/color]
[list]
[*][color=blue]新增图片支持[/color]
[*][color=blue]新增音频支持[/color]
[*][color=blue]整体构筑支持[/color]
[*][color=blue]实现自定义样式[/color]
[*][color=blue]响应式设计[/color]
[*][color=blue]跨平台支持[/color]
[list]
[*][color=blue]核心：Web[/color]
[*][color=blue]Windows[/color]
[*][color=blue]Android[/color]
[/list][/list][*][color=gray]【游戏逻辑】建立一个基于EraSQN的TJ系统，但对其中的各项基本评价状态进行推倒重写。[/color]
[/list]


[align=center][size=4][b]游戏截图[/b][/size]
[attachimg]1034[/attachimg]
[attachimg]1035[/attachimg]
[attachimg]1041[/attachimg]
[attachimg]1042[/attachimg]
[attachimg]768[/attachimg]
[attachimg]685[/attachimg]
[attachimg]687[/attachimg]
[attachimg]689[/attachimg]
[attachimg]690[/attachimg]
[attachimg]691[/attachimg]


[size=4][b]游戏更新[/b][/size]
[backcolor=yellow]“战斗原型”[/backcolor]更新：[backcolor=yellow]v0.1.0-190630+fix[/backcolor]
下次更新时间：[backcolor=yellow]2020 Q1?
注意：功能预览版仅作为讨论作品，不保证游戏性。[/backcolor]

下次更新内容（拟）：
1. 新前端
2. 新引擎
3. 更多游戏机制/内容


[hide=1]
[color=red]→如果您看到了这行字，那么您已经具有了浏览权限←[/color]
[table=70%]
[tr][td=5,1][align=center][b]EraLife[/b][/align][/td][/tr]
[tr][td][align=center][b]平台类型[/b][/align][/td][td][align=center][b]平台架构[/b][/align][/td][td][align=center][b]解决方案[/b][/align][/td][td][align=center][b]下载地址[/b][/align][/td][/tr]
[tr][td=1,5][align=center][b]桌面端[/b][/align][/td][/tr]
[tr][td][align=center][b]Windows[/b] 64-bit[/align][/td][td][align=center]EraLife-amd64[/align][/td][td][align=center][url=https://pan.baidu.com/s/1B_Iyvi5ewuHhgevzd_PYaQ]l8pp[/url][/align][/td][/tr]
[tr][td][align=center][b]Windows[/b] 32-bit[/align][/td][td][align=center]EraLife-i386[/align][/td][td][align=center]Coming Soon[/align][/td][/tr]
[tr][td][align=center][b]Linux[/b][/align][/td][td][align=center]EraLife-amd64[/align][/td][td][align=center]Coming Soon[/align][/td][/tr]
[tr][td][align=center][b]OS X[/b][/align][/td][td][align=center]No Plan[/align][/td][td][align=center]No Plan[/align][/td][/tr]
[tr][td=1,3][align=center][b]移动端[/b][/align][/td][/tr]
[tr][td][align=center][b]Android[/b][/align][/td][td][align=center]Coming Soon[/align][/td][td][align=center]Coming Soon[/align][/td][/tr]
[tr][td][align=center][b]iOS[/b][/align][/td][td][align=center]No Plan[/align][/td][td][align=center]No Plan[/align][/td][/tr]
[tr][td=5,1][align=center][b]EraLife Next[/b][/align][/td][/tr]
[tr][td][align=center][b]平台类型[/b][/align][/td][td][align=center][b]平台架构[/b][/align][/td][td][align=center][b]解决方案[/b][/align][/td][td][align=center][b]下载地址[/b][/align][/td][/tr]
[tr][td][align=center][b]Web[/b][/align][/td][td][align=center][b]Broswer[/b][/align][/td][td][align=center]EraLife-core[/align][/td][td][align=center]Coming Soon[/align][/td][/tr]
[tr][td][align=center][b]Server[/b][/align][/td][td][align=center][b]Server[/b][/align][/td][td][align=center]EraLife-server[/align][/td][td][align=center]Coming Soon[/align][/td][/tr]
[/table][color=red]→如果您看到了这行字，那么您已经具有了浏览权限←[/color]
[/hide]


[size=4][b]相关链接[/b][/size][/align]
[list]
[*]游戏催更：[url=https://jq.qq.com/?_wv=1027&k=54G9nwm]940068054[/url]（口令：era）
[*]引擎支持：[url=https://jq.qq.com/?_wv=1027&k=59zsY8p]940122876[/url]（口令：era）
[*][url=https://tieba.baidu.com/p/5913297784]EraLife 贴吧更新贴[/url]（现已停止更新）
[/list][/size]