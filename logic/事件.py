import erajs.api as a


class EventManager:
    """
    一个游戏（Game）里有多个宇宙（Universe）；默认数量为1
    一个宇宙（Universe）里有多个星球（Star）；默认数量为1
    一个星球（Star）里有多个地图（Map）；默认数量为1
    一个地图（Map）里有多个区域（Area）；
    一个区域（Area）里有多个房屋（House）或地点（Place）；
    一个房屋（House）里有多个房间（Room），地点（Place）不可再细分；
    同一个游戏中可以存在不同的世界，不同的世界可以拥有不同的地点、不同的人等等。
    """
    pass
