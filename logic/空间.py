import random

import erajs.api as a
from . import 姓名 as xm


class SpaceManager:
    """
    一个游戏（Game）里有多个宇宙（Universe）；默认数量为1
    一个宇宙（Universe）里有多个星球（Star）；默认数量为1
    一个星球（Star）里有多个地图（Map）；默认数量为1
    一个地图（Map）里有多个区域（Area）；
    一个区域（Area）里有多个地点（Place）；
    一个地点（Place）里或有房屋（House）；
    一个房屋（House）里有多个房间（Room）。
    以上概念均含有person属性，表示任务存在于此切不存在于任何child中。
    同一个游戏中可以存在不同的世界，不同的世界可以拥有不同的地点、不同的人等等。
    """

    def __init__(self):
        self.data = {}

    def init(self):
        """
        1. 生成Map
        2. 向下生成
        3. 向上生成
        """
        self.new_space()

    def new_space(self):
        """代替new_world"""
        # 实例化空间、宇宙、星球、和一张地图
        space = Space()
        universe = Universe()
        star = Star()
        m = Map()
        m.generate_map()
        # 等待直到地图生成
        while True:
            if 'MAP_GENERATED' in a.data:
                del a.data['MAP_GENERATED']
                break
        # 组装各个概念
        star.add_map(m)
        universe.add_star(star)
        space.add_universe(universe)
        self.data['space'] = space
        # 生成地点
        area_in_a_map = []  # 地图内拥有的地点
        for each in a.data['data.区域']:
            count_max = random.randint(
                a.data['data.区域'][each]['min'], a.data['data.区域'][each]['max'])
            for i in range(a.data['data.区域'][each]['min'], count_max+1):
                area_in_a_map.append(each)
        area_in_a_map = random.sample(
            area_in_a_map, len(area_in_a_map))  # 随机化列表
        area_nodes = []
        for area in area_in_a_map:  # 将列表替换成节点
            area_node = {
                'name': xm.get_random_name()+area,  # 随机一个地点名称
                'type': area,  # 地点类型
                'child': a.data['data.区域'][area]['地点']
            }
            area_nodes.append(area_node)
        # 将数据推入游戏环境
        m.change_random_area(area_nodes)
        # 加入房间
        # 生成人物
        # 生成主角
        # birth_area = self.get_birth_area()
        # self.add_person_to_area(person, birth_area)
        # self.save()
        # self.load()

    def add_person_to_area(self, person, area):
        pass

    def save(self):
        """将数据保存至db（代替load_all_world）"""
        space_data = self.data['space'].save()
        a.data['db']['space'] = space_data

    def load(self):
        """从db加载数据"""
        space = Space()
        space.load(a.data['db']['space'])
        self.data['space'] = space

    def get_world(self):  # ？
        """代替get_world"""
        pass

    def change_area(self):
        pass

    def find_person(self, hash):
        """查找人物"""
        result = self.data['space'].find_person(hash)
        return result

    def find_player(self):
        """查找主角"""
        return self.find_person(a.data['db']['player'])

    def find_birth_place(self):
        """代替find_birth_place"""
        pass

    def add_to_birth_area(self, hash):
        """代替add_to_birth_place"""
        def find_birth(child):
            if '出生区域' in child.data:
                if child.data['出生区域']:  # 返回当前项
                    if 'hash' in child.data:
                        return [child.data['hash']]
                    elif 'i' in child.data:
                        return [child.data['i']]
                    else:
                        a.warn('no i & hash')
                        return []
                else:
                    return []  # 返回空
            else:
                for each in child.data['child']:
                    result = find_birth(each)
                    if result == []:
                        continue
                    else:
                        r = [child.data['hash']]
                        r.extend(result)
                        return r
                return []
        birth_area = find_birth(self.data['space'])
        self.get(birth_area).data['person'].append(hash)
        print(self.find_person(hash))

    def get(self, index):
        target = self.data['space']
        for each in index[1:]:
            candidates = target.data['child']
            if isinstance(each, str):
                for every in candidates:
                    if every.data['hash'] == each:
                        target = every
                        break
            elif isinstance(each, int):
                for every in candidates:
                    if every.data['i'] == each:
                        target = every
                        break
        return target

    def get_index(self, pos):
        def find_pos(current_pos):
            if 'hash' in pos.data:
                if 'hash' in current_pos.data:
                    if pos.data['hash'] == current_pos.data['hash']:
                        return [current_pos.data['hash']]
            elif 'i' in pos.data:
                if 'i' in current_pos.data:
                    if pos.data['i'] == current_pos.data['i']:
                        return [current_pos.data['i']]
            if 'child' in current_pos.data:
                for each in current_pos.data['child']:
                    result = find_pos(each)
                    if not result == []:
                        if 'hash' in current_pos.data:
                            new_result = [current_pos.data['hash']]
                        elif 'i' in current_pos.data:
                            new_result = [current_pos.data['i']]
                        new_result.extend(result)
                        return new_result
            return []
        return find_pos(self.data['space'])

    def add_to_place(self):
        """代替add_to_place"""
        pass

    def find_all_person_in_space(self, index):
        """代替find_all_person_in_place"""
        person_list = []
        target = self.data['space']
        for each in index[1:]:
            candidates = target.data['child']
            if isinstance(each, str):
                for every in candidates:
                    if every.data['hash'] == each:
                        target = every
                        break
            elif isinstance(each, int):
                for every in candidates:
                    if every.data['i'] == each:
                        target = every
                        break
        for person_hash in target.data['person']:
            person_list.append(a.data['cm'].get_person(person_hash))
        return person_list

    def enter(self):
        """进入子区域"""
        pass

    def leave(self, person_hash):
        """退出到父区域"""
        last_pos_index = self.find_person(person_hash)
        if len(last_pos_index) == 8:
            new_pos_index = last_pos_index[:-2]
        else:
            new_pos_index = last_pos_index[:-1]
        pos = self.get(last_pos_index)
        pos.data['person'].remove(person_hash)
        new_pos = self.get(new_pos_index)
        new_pos.data['person'].append(person_hash)  # 房间小心

    def move_to(self, person_hash, target):
        """将人物移动至某地"""
        pos = self.get(self.find_person(person_hash))
        pos.data['person'].remove(person_hash)
        if isinstance(target, House):
            for each in target.data['child']:
                if 'entry' in each.data['n']:
                    each.data['person'].append(person_hash)
                    break
        else:
            target.data['person'].append(person_hash)

    def get_neighbours(self, area):
        neighbours = []
        index = self.get_index(area)
        for each in area.data['n']:
            new_index = index[:-1]
            new_index.append(each)
            neighbours.append(self.get(new_index))
        return neighbours


class Space:
    def __init__(self):
        self.data = {
            'hash': a.new_hash(),
            'child': [],
            'admin': '',
            'person': []
        }

    def add_universe(self, universe):
        self.data['child'].append(universe)

    def save(self):
        data = dict(self.data)
        data['child'] = []
        for universe in self.data['child']:
            data['child'].append(universe.save())
        return data

    def load(self, data):
        self.data = dict(data)
        self.data['child'] = []
        for universe_data in data['child']:
            universe = Universe()
            universe.load(universe_data)
            self.data['child'].append(universe)

    def find_person(self, hash):
        """查找人物"""
        result = []
        if hash in self.data['person']:
            result.append(self.data['hash'])
        else:
            for each in self.data['child']:
                l = each.find_person(hash)
                if len(l) > 0:
                    result.append(self.data['hash'])
                    result.extend(l)
                    break
        return result


class Universe:
    def __init__(self):
        self.data = {
            'hash': a.new_hash(),
            'child': [],
            'admin': '',
            'person': []
        }

    def add_star(self, star):
        self.data['child'].append(star)

    def save(self):
        data = dict(self.data)
        data['child'] = []
        for star in self.data['child']:
            data['child'].append(star.save())
        return data

    def load(self, data):
        self.data = dict(data)
        self.data['child'] = []
        for star_data in data['child']:
            star = Star()
            star.load(star_data)
            self.data['child'].append(star)

    def find_person(self, hash):
        """查找人物"""
        result = []
        if hash in self.data['person']:
            result.append(self.data['hash'])
        else:
            for each in self.data['child']:
                l = each.find_person(hash)
                if len(l) > 0:
                    result.append(self.data['hash'])
                    result.extend(l)
                    break
        return result


class Star:
    def __init__(self):
        self.data = {
            'hash': a.new_hash(),
            'child': [],
            'admin': '',
            'person': []
        }

    def add_map(self, m):
        self.data['child'].append(m)

    def save(self):
        data = dict(self.data)
        data['child'] = []
        for m in self.data['child']:
            data['child'].append(m.save())
        return data

    def load(self, data):
        self.data = dict(data)
        self.data['child'] = []
        for m_data in data['child']:
            m = Map()
            m.load(m_data)
            self.data['child'].append(m)

    def find_person(self, hash):
        """查找人物"""
        result = []
        if hash in self.data['person']:
            result.append(self.data['hash'])
        else:
            for each in self.data['child']:
                l = each.find_person(hash)
                if len(l) > 0:
                    result.append(self.data['hash'])
                    result.extend(l)
                    break
        return result


class Map:
    def __init__(self):
        self.data = {
            'hash': a.new_hash(),
            'child': [],
            'admin': '',
            'person': []
        }

    def generate_map(self):
        a.dispatch_event('SEND', value={
            'type': 'generate_map',
            'value': {},
            'from': 'b',
            'to': 'r'
        })

        def handle_raw_map_data(data):
            for raw_area_data in data['value']:
                area = Area()
                area.parse(raw_area_data)
                self.data['child'].append(area)

            a.data['MAP_GENERATED'] = ''
        a.add_listener('MAP_GENERATED', handle_raw_map_data)

    def save(self):
        data = dict(self.data)
        data['child'] = []
        for area in self.data['child']:
            data['child'].append(area.save())
        return data

    def load(self, data):
        self.data = dict(data)
        self.data['child'] = []
        for area_data in data['child']:
            area = Area()
            area.load(area_data)
            self.data['child'].append(area)

    def find_person(self, hash):
        """查找人物"""
        result = []
        if hash in self.data['person']:
            result.append(self.data['hash'])
        else:
            for each in self.data['child']:
                l = each.find_person(hash)
                if len(l) > 0:
                    result.append(self.data['hash'])
                    result.extend(l)
                    break
        return result

    def change_random_area(self, nodes):
        area_on_water = []
        for each in self.data['child']:
            if each.data['h'] >= 0:
                area_on_water.append(each)
        target = random.sample(range(0, len(area_on_water)), len(nodes))
        for i, node in enumerate(nodes):
            area_on_water[target[i]].add_to_data(node)
            # TODO: 发送信息到前端


class Area:
    def __init__(self):
        self.data = {
            'child': [],
            'admin': '',
            'person': []
        }

    def add_place(self, place):
        self.data['child'].append(place)

    def add_place_data(self, place_data):
        for each in place_data:
            place = Place(each)
            self.data['child'].append(place)

    def save(self):
        data = dict(self.data)
        data['child'] = []
        for place in self.data['child']:
            data['child'].append(place.save())
        return data

    def load(self, data):
        self.data = dict(data)
        self.data['child'] = []
        for place_data in data['child']:
            place = Place()
            place.load(place_data)
            self.data['child'].append(place)

    def parse(self, data):
        if -2000 < data['h'] <= -1000:
            self.data['地形'] = '深海'
        elif -1000 < data['h'] <= -100:
            self.data['地形'] = '浅海'
        elif -100 < data['h'] <= 0:
            self.data['地形'] = '海滩'
        elif 0 < data['h'] <= 100:
            self.data['地形'] = '沙滩'
        elif 100 < data['h'] <= 2000:
            self.data['地形'] = '平原'
        elif 2000 < data['h'] <= 4000:
            self.data['地形'] = '森林'
        elif 4000 < data['h'] <= 6000:
            self.data['地形'] = '高原'
        elif 6000 < data['h'] <= 7000:
            self.data['地形'] = '裸岩'
        elif 7000 < data['h'] <= 8000:
            self.data['地形'] = '雪峰'
        # print('x', data['x'], 'y', data['y'], 'h', data['h'])
        for key in data:
            self.data[key] = data[key]

    def find_person(self, hash):
        """查找人物"""
        result = []
        if hash in self.data['person']:
            result.append(self.data['i'])
        else:
            for each in self.data['child']:
                l = each.find_person(hash)
                if len(l) > 0:
                    if 'hash' in self.data:
                        result.append(self.data['hash'])
                    elif 'i' in self.data:
                        result.append(self.data['i'])
                    result.extend(l)
                    break
        return result

    def add_to_data(self, d):
        for key in d:
            if key == 'child':
                self.add_place_data(d[key])
            elif key == 'type':
                self.data[key] = d[key]
                self.data['出生区域'] = a.data['data.区域'][d[key]]['出生区域']
            else:
                self.data[key] = d[key]


class Place:
    def __init__(self, type=''):
        self.data = {
            'hash': a.new_hash(),
            'child': [],
            'admin': '',
            'person': [],
            'type': type,
        }
        if not type == '':
            for house_type in a.data['data.地点'][type]['默认房屋']:
                house = House(house_type)
                self.data['child'].append(house)

    def save(self):
        data = dict(self.data)
        data['child'] = []
        for house in self.data['child']:
            data['child'].append(house.save())
        return data

    def load(self, data):
        self.data = dict(data)
        self.data['child'] = []
        for house_data in data['child']:
            house = House()
            house.load(house_data)
            self.data['child'].append(house)

    def find_person(self, hash):
        """查找人物"""
        result = []
        if hash in self.data['person']:
            result.append(self.data['hash'])
        else:
            for each in self.data['child']:
                l = each.find_person(hash)
                if len(l) > 0:
                    result.append(self.data['hash'])
                    result.extend(l)
                    break
        return result


class House:
    def __init__(self, type=''):
        self.data = {
            'hash': a.new_hash(),
            'child': [],
            'admin': '',
            'type': type,
        }
        if not type == '':
            for room_name in a.data['data.房屋'][type]['房间']:
                room = Room(
                    room_name, a.data['data.房屋'][type]['房间'][room_name])
                self.data['child'].append(room)
            # 加入管理员
            admin = a.data['cm'].new_random_person()
            for key in a.data['data.物品']:
                admin.data['物品'].append(a.data['it'].new_item(key))
            self.data['child'][0].add_person(admin)

    def add_room(self, room):
        self.data['child'].append(room)

    def save(self):
        data = dict(self.data)
        data['child'] = []
        for room in self.data['child']:
            data['child'].append(room.save())
        return data

    def load(self, data):
        self.data = dict(data)
        self.data['child'] = []
        for room_data in data['child']:
            room = Room()
            room.load(room_data)
            self.data['child'].append(room)

    def find_person(self, hash):
        """查找人物"""
        result = []
        for each in self.data['child']:
            l = each.find_person(hash)
            if len(l) > 0:
                result.append(self.data['hash'])
                result.extend(l)
                break
        return result


class Room:
    def __init__(self, name='', data={}):
        if not data == {}:
            self.data = {
                'hash': a.new_hash(),
                'admin': '',
                'person': [],
                'name': name,
                'n': data['n']
            }

    def save(self):
        data = dict(self.data)
        return data

    def load(self, data):
        self.data = dict(data)

    def find_person(self, hash):
        """查找人物"""
        result = []
        if hash in self.data['person']:
            result.append(self.data['hash'])
        return result

    def add_person(self, person):
        if isinstance(person, str):
            self.data['person'].append(person)
        else:
            self.data['person'].append(person.data['hash'])

    def is_entry(self):
        return 'entry' in self.data['n']
