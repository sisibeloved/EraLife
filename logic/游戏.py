import erajs.api as a

import logic.调试 as Debug
import logic.时间 as Time
import logic.空间 as Space
import logic.任务 as Mission
import logic.互动 as Interaction
import logic.事件 as Event
import logic.物品 as Item
import logic.技能 as Skill
import logic.世界 as sj
import logic.生物 as Creature
import logic.战斗 as zd
import logic.口上 as ks
import logic.wiki as wiki


class GameManager():
    """
    游戏管理器是唯一的，负责管理各个分管理器。
    管理器架构：
    游戏管理器
      ├─调试管理器：提供游戏调试功能
      ├─时间管理器：管理时间（When）
      ├─空间管理器：管理空间（Where）
      ├─生物管理器：管理生物（Who）
      ├─任务管理器：管理任务（Why）
      ├─互动管理器：管理互动（How）
      ├─事件管理器：管理事件（What）
      ├─文本管理器：管理文本
    """

    def __init__(self):  # 结构初始化
        worlds = {}
        # 初始化调试管理器√
        a.data['dm'] = Debug.DebugManager()
        # 初始化时间管理器√
        a.data['tim'] = Time.TimeManager()
        # 初始化空间管理器√
        a.data['sm'] = Space.SpaceManager()
        # 初始化生物管理器√
        a.data['cm'] = Creature.CreatureManager()
        # a.data['am'] = Animal.AnimalManager()
        # 初始化物品管理器√
        a.data['it'] = Item.ItemManager()
        # 初始化任务管理器√
        a.data['mm'] = Mission.MissionManager()
        # 初始化互动管理器√
        a.data['im'] = Interaction.InteractionManager()
        # 初始化事件管理器√
        a.data['em'] = Event.EventManager()
        a.data['skm'] = Skill.SkillManager()
        ##################################
        # 初始化世界管理器×
        # a.data['wm'] = sj.WorldManager()
        # 初始化人物管理器×
        # a.data['pm'] = rw.PersonManager()
        # 初始化战斗管理器×
        a.data['bm'] = zd.BattleManager()
        # 初始化口上管理器×
        a.data['km'] = ks.KojoManager()
        # 初始化Wiki×
        a.data['wiki'] = wiki.Wiki()

    def init(self):  # 内容初始化
        # 初始化时间管理器
        a.data['tim'].init()
        # 初始化空间管理器
        a.data['sm'].init()
        # 初始化世界管理器
        # a.data['wm'].init()
        # 初始化人物管理器
        # a.data['pm'].init()
        # 初始化人物管理器
        a.data['it'].init()
        # 初始化战斗管理器
        a.data['bm'].init()
        # 初始化互动管理器
        # a.data['im'].init()
        # 初始化口上管理器
        a.data['km'].init()
        # 初始化Wiki
        a.data['wiki'].init()
        # world = a.data['wm'].new_world()
        # world.save()
        # a.data['pm'].generate_init_person()
