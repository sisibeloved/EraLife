import random

import erajs.api as a


class ItemManager:
    def __init__(self):
        a.data['db']['item'] = []
        self.data = {
            'child': []
        }

    def init(self):
        pass

    def new_item(self, type):
        item = Item(type)
        self.data['child'].append(item)
        return item.data['hash']

    def get_item(self, hash):
        if hash == '':
            return None
        for item in self.data['child']:
            if item.data['hash'] == hash:
                return item
        return

    def load(self):
        self.data = a.data['db']['item']
        self.data['child'] = []
        for item_data in a.data['db']['item']['child']:
            item = Item()
            item.load(item_data)
            self.data['child'].append(item)

    def save(self):
        save_data = dict(self.data)
        save_data['child'] = []
        for item in self.data['child']:
            save_data['child'].append(item.data)
        a.data['db']['item'] = save_data
        print(save_data)


class Item:
    def __init__(self, type=None):
        if type == None:
            self.data = {}
        else:
            self.data = {
                'hash': a.new_hash(),
                'type': type
            }
            for key in a.data['data.物品'][type]:
                self.data[key] = a.data['data.物品'][type][key]
            if 'name' not in a.data['data.物品'][type]:
                self.data['name'] = self.data['type']

    def load(self, data):
        self.data = data
