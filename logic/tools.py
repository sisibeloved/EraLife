
import erajs.api as a


def game_tools():
    a.page()
    a.h('游戏工具')
    a.t()
    a.b('人物模拟器')
    a.t()
    a.b('互动模拟器', a.goto, interaction_simulator, popup='用于模拟战斗过程，改良战斗体验')
    a.t()
    a.b('返回', a.back)


def interaction_simulator():
    def main():
        """主界面"""
        a.page()
        a.h('互动模拟器')
        a.mode('grid', 7, celled=True)
        for y in range(6):
            for x in range(7):
                unit = interaction.get_unit_in_pos(x, y)
                if y == 2 or y == 3:  # 中间两排不放置单位
                    pass
                elif unit == None:  # 没有单位在的格子
                    a.b('+', a.goto, add_unit, x, y)
                elif unit[0].is_player():  # 单位为自己操控：绿色
                    a.l(unit[0].data['系统称呼'],
                        ui_change_unit, x, y, color='#0f0')
                elif unit[1]['team'] == 1:  # 单位为盟友:蓝色
                    a.l(unit[0].data['系统称呼'],
                        ui_change_unit, x, y, color='#00f')
                elif unit[1]['team'] == 0:  # 单位为敌人:红色
                    a.l(unit[0].data['系统称呼'],
                        ui_change_unit, x, y, color='#f00')
                a.t()
        a.mode()
        a.t()
        a.b('开始模拟', interaction.start)
        a.t()
        a.b('返回', a.back)

    def add_unit(x, y):
        """界面：加入生物"""
        def ui_add_unit():
            def change_type(type):
                """回调：改变人物/动物"""
                nonlocal new_unit, new_unit_type, is_player
                if type == '人物':
                    new_unit = a.data['cm'].new_random_person()
                elif type == '动物':
                    new_unit = a.data['cm'].new_random_animal()
                is_player = False
                new_unit_type = type
                a.clear(1)
                a.repeat()

            def change_team(team_num):
                """回调：改变单位队伍"""
                nonlocal new_unit_team
                new_unit_team = int(team_num)

            def change_is_player(data):
                """回调：改变是否为玩家操控"""
                nonlocal is_player
                if data == '是':
                    is_player = True
                else:
                    is_player = False

            def confirm():
                """回调：确定新增角色"""
                if 'player' not in a.data['db']:
                    a.data['db']['player'] = ''
                if is_player:
                    a.data['db']['player'] = new_unit.data['hash']
                interaction.add_unit(new_unit, pos=(x, y), team=new_unit_team)
                a.back()

            def change_name(name):
                """回调：更改单位名称"""
                new_unit.data['name'] = name

            def change_gender(gender):
                """回调：更改单位性别"""
                new_unit.data['gender'] = gender
            a.page()
            a.h('新增单位：{}'.format(new_unit_type))
            a.t()
            a.t('类型：　　')
            a.b('人物', change_type, '人物', disabled=new_unit_type == '人物')
            a.t('/')
            a.b('动物', change_type, '动物', disabled=new_unit_type == '动物')
            a.t()
            a.t('队伍：　　')
            a.radio(['0', '1'], new_unit_team, change_team)
            a.t()
            a.t('玩家操控：')
            a.radio(['是', '否'], 1, change_is_player)
            a.t()
            a.t('姓名：　　')
            a.input(change_name, new_unit.data['系统称呼'])
            a.t()
            a.t('性别：　　')
            gender_list = list(a.data['data.others']['性别'].keys())
            a.radio(gender_list, gender_list.index(
                new_unit.data['gender']), change_gender)
            a.t()
            a.b('属性管理', a.goto, ui_attri_manager, new_unit)
            a.b('技能管理', a.goto, ui_skill_manager, new_unit)
            a.b('装备管理', a.goto, ui_equip_manager, new_unit)
            a.t()
            a.b('确定', confirm)
            a.b('返回', a.back)
        # 新增人物界面初始化
        new_unit = a.data['cm'].new_random_person()
        new_unit_type = '人物'
        if y <= 2:
            new_unit_team = 0
        elif 3 <= y:
            new_unit_team = 1
        is_player = False
        a.clear_gui(1)
        a.goto(ui_add_unit)

    def ui_change_unit(x, y):
        """界面：修改生物"""
        pass

    def ui_attri_manager(unit):
        """界面：属性管理"""
        def ui_add_attr():
            pass
        a.page()
        a.h('属性管理:{}'.format(
            unit.data['系统称呼']
        ))
        a.t()
        a.t('力量:{}　敏捷:{}　智力:{}　魅力:{}'.format(
            unit.data['力量'],
            unit.data['敏捷'],
            unit.data['智力'],
            unit.data['魅力']
        ))
        a.t()
        a.t('未分配属性点:{}　'.format(
            unit.data['属性点']
        ))
        a.b('分配属性点', ui_add_attr,
            disabled=True if unit.data['属性点'] > 0 else False)
        a.t()
        a.b('返回', a.back)

    def ui_skill_manager(unit):
        """界面：技能管理"""
        a.page()
        a.h('技能管理')
        a.t()
        a.b('返回', a.back)

    def ui_equip_manager(unit):
        """界面：装备管理"""
        def show_button(pos):
            if unit.data[pos] == '':
                a.l('{}:空'.format(pos), a.goto, change_equipment, pos)
            else:
                item_name = a.data['it'].get_item(
                    unit.data[pos]).data['name']
                a.l('{}:{}'.format(pos, item_name),
                    a.goto, change_equipment, pos)

        def change_equipment(pos):
            a.page()
            a.t('{}修改为：'.format(pos))
            a.t()
            a.b('无', confirm, pos, None)
            for item_hash in unit.data['物品']:
                item = a.data['it'].get_item(item_hash)
                if pos in item.data['部位']:
                    a.b(item.data['name'], confirm, pos, item)
            a.t()
            a.b('返回', a.back)

        def confirm(pos, item):
            a.page()
            if item == None:
                a.t('确定要空出{}装备吗？'.format(pos))
            else:
                a.t('确定要给{}装备{}吗？'.format(pos, item.data['name']))
            a.t()
            a.b('是', change, pos, item)
            a.t('/')
            a.b('否', a.back)

        def change(pos, item):
            a.page()
            if item == None:
                unit.data[pos] = ''
                a.t('你空出了{}装备。'.format(pos), True)
            else:
                unit.data[pos] = item.data['hash']
                a.t('你已给{}装备了{}。'.format(pos, item.data['name']), True)
            a.back()
        a.page()
        a.h('装备管理')
        a.mode('grid', 7)
        for i in range(3):
            a.t()
        show_button('头部')
        for i in range(7):
            a.t()
        show_button('面部')
        for i in range(7):
            a.t()
        show_button('颈部')
        a.t()
        a.t()
        show_button('手部')
        for i in range(3):
            a.t()
        show_button('右手')
        a.t()
        show_button('右指')
        a.t()
        show_button('上身')
        a.t()
        show_button('左指')
        a.t()
        show_button('左手')
        for i in range(5):
            a.t()
        show_button('下身')
        for i in range(7):
            a.t()
        show_button('脚部')
        for i in range(3):
            a.t()
        a.mode()
        a.t()
        a.b('返回', a.back)
    # 进入互动模式的初始化
    your_team = []
    my_team = []
    interaction = a.data['im'].new()
    a.clear_gui(1)
    a.goto(main)
