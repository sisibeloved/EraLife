# coding:utf-8
import time
import random
import erajs.api as a
import logic.main as m
import logic.editor as editor
import logic.tools as tools
import logic.游戏 as yx
import logic.世界 as sj
import logic.空间 as kj
import logic.生物 as sw
# 注意！不推荐在此处定义变量，而应在 a.init() 之后将变量定义在 a.data['db'] 中，以获得数据持久性支持。

version = '0.1.0-190302'
aka = '“2019新年更迭”'


def cover():
    a.title('EraLife v{} with Era.js v{}'.format(version, a.version))
    a.debug(123)
    a.page()
    a.mode('grid', 1)
    a.t()
    a.h('EraLife')
    a.t()
    a.t('v{}　aka {}'.format(version, aka))
    a.t()
    a.t('with Era.js v{}　aka {}'.format(a.version, a.aka))
    for i in range(4):
        a.t()
    a.b('开始游戏', a.goto, start_new_game)
    a.t()
    a.b('加载游戏', a.goto, load_game)
    a.t()
    a.b('游戏设置', a.goto, game_setting)
    a.t()
    a.b('游戏工具', a.goto, tools.game_tools)
    a.t()
    a.b('游戏编辑器', a.goto, editor.cover)
    a.t()
    a.b('退出游戏', a.exit)
    a.t()


def start_new_game():
    """开始新游戏"""
    a.page()
    a.h('请选择游戏模式')
    a.t()
    a.b('默认游戏模式', a.goto, default_mode)
    a.t()
    a.b('自定义游戏模式', None, disabled=True)
    a.t()
    a.b('返回', a.back)


def generate_map():
    def callback(data):
        for each in data['value']:
            print(each)
        print(len(data['value']))
        a.data['db'][map]
    a.generate_map()
    a.add_listener('MAP_GENERATED', callback)


def default_mode():
    """应用默认游戏模式"""
    a.page()
    a.t('正在初始化游戏世界，请等待…')
    time.sleep(1)
    # TODO: 优化游戏初始化逻辑
    a.data['gm'].init()
    a.t()
    a.t('默认游戏世界初始化完毕！')
    time.sleep(1)
    # TODO: 显示游戏默认设置内容
    # 初始化世界
    a.page()
    a.h('请选择玩家创建方式')
    a.t()
    a.b('选择预设角色', a.goto, ui_choose_preset_player, popup='选择专为开局设计的角色')
    a.t()
    a.b('选择游戏内角色', None, disabled=True, popup='使用游戏内的常规角色')
    a.t()
    a.b('自定义角色', a.goto, ui_custom_player, disabled=True, popup='通过向导一步步定制游戏角色')
    a.t()
    a.b('从其他存档加载角色', None, disabled=True, popup='使用其他存档内的角色')
    a.t()
    a.b('返回', a.back)


def ui_choose_preset_player():
    def apply_person(name):
        player = a.data['cm'].new_null_person()
        for attr in a.data['data.预设玩家角色'][name]:
            player.data[attr] = a.data['data.预设玩家角色'][name][attr]
        a.data['db']['player'] = player.data['hash']
        player.update()
        a.data['sm'].add_to_birth_area(player.data['hash'])
        a.page()
        a.t('已设定！')
        time.sleep(1)
        a.goto(ask_intro)

    a.page()
    a.h('选择预设玩家角色')
    a.t()
    for person_name in a.data['data.预设玩家角色']:
        a.b('选择', apply_person, person_name)
        a.t(person_name)
        a.t()
    a.b('返回', a.back)


def ui_custom_player():
    """
    性别
    种族
    外貌
    职业
    名称
    """
    def change_page(page_func):
        a.clear_gui(1)
        a.goto(page_func)

    def show_text(name):  # 显示某一领域的文本
        for line in a.data['data.others']['自定义角色文本'][name]:
            a.t(line)
            a.t()

    def ui_choose_sex():
        def set_sex(sex):
            nonlocal player
            player.data['gender'] = sex
        a.page()
        show_text('性别')
        a.radio(list(a.data['data.others']['性别'].keys()), func=set_sex)
        a.t()
        a.b('返回', a.back)
        a.b('继续', change_page, ui_choose_race)

    def ui_choose_race():
        def set_race(race):
            nonlocal player
            player.data['种族'] = race
        a.page()
        show_text('种族')
        a.t('请选择种族：')
        a.dropdown(list(a.data['data.种族'].keys()), func=set_race)
        a.t()
        a.b('返回', a.back)
        a.b('继续', change_page, ui_choose_job)

    def ui_choose_job():
        def set_job(job):
            nonlocal player
            player.data['职业'] = job
        a.page()
        show_text('职业')
        a.dropdown(list(a.data['data.职业'].keys()), func=set_job)
        a.t()
        a.b('返回', a.back)
        a.b('继续', change_page, ui_set_name)

    def ui_set_name():
        def set_name(data):
            player.data['name'] = data

        def set_sys_name(data):
            player.data['系统称呼'] = data

        def set_self_name(data):
            player.data['自称'] = data

        def set_resp_name(data):
            player.data['尊称'] = data

        def set_diss_name(data):
            player.data['蔑称'] = data
        a.page()
        show_text('名称')
        a.t('自定义名称:')
        a.input(set_name, player.data['name'])
        a.t()
        a.t('自定义系统称呼:')
        a.input(set_sys_name, player.data['系统称呼'])
        a.t()
        a.t('自定义自称:')
        a.input(set_self_name, player.data['自称'])
        a.t()
        a.t('自定义尊称:')
        a.input(set_resp_name, player.data['尊称'])
        a.t()
        a.t('自定义蔑称:')
        a.input(set_diss_name, player.data['蔑称'])
        a.t()
        a.b('返回', a.back)
        a.b('继续', change_page, ui_confirm)

    def ui_confirm():
        a.page()
        player.data['money'] = 100
        player.update()
        player.reset()
        a.data['cm'].show_person_profile(player)
        a.t()
        a.t('确定吗？')
        a.t()
        a.b('是', add_person)
        a.t('/')
        a.b('否', a.back)

    def add_person():
        player.save()
        a.data['cm'].add_person(player)
        a.data['db']['player'] = player.data['hash']
        a.data['wm'].add_to_birth_place(player.data['hash'])
        a.page()
        a.t('已设定！', True)
        a.goto(ask_intro)

    player = a.data['cm'].new_null_person()
    player.data['name'] = '亚当'
    player.data['系统称呼'] = '你'
    player.data['自称'] = '我'
    player.data['尊称'] = '你'
    player.data['蔑称'] = '你'
    change_page(ui_choose_sex)


def custom_player():
    def gui_set_name():
        a.page()
        a.h('请设置人物名称&称呼')
        a.t()
        a.t('默认名称:【{}】　自定义:'.format(player.data['name']))
        a.input(set_name)
        a.t()
        a.t('默认系统称呼:【{}】　自定义:'.format(player.data['系统称呼']))
        a.input(set_sys_name)
        a.t()
        a.t('默认自称:【{}】　自定义:'.format(player.data['自称']))
        a.input(set_self_name)
        a.t()
        a.t('默认尊称:【{}】　自定义:'.format(player.data['尊称']))
        a.input(set_resp_name)
        a.t()
        a.t('默认蔑称:【{}】　自定义:'.format(player.data['蔑称']))
        a.input(set_diss_name)
        a.t()
        a.b('确定', set_attr)

    def set_name(data):
        player.data['name'] = data

    def set_sys_name(data):
        player.data['系统称呼'] = data

    def set_self_name(data):
        player.data['自称'] = data

    def set_resp_name(data):
        player.data['尊称'] = data

    def set_diss_name(data):
        player.data['蔑称'] = data

    def set_attr():
        a.page()
        a.h('请设置人物属性')
        a.t()
        a.t('性别：')
        a.radio(['男', '女'], func=set_gender)
        a.t()
        a.t('身高：')
        a.radio(['矮个', '普通', '高个'], 1, set_height)
        a.t()
        a.t('体型：')
        a.radio(['瘦弱', '苗条', '普通', '丰满', '肥胖'], 2, set_build)
        a.t()
        a.t('体质：')
        a.radio(['虚弱', '普通', '强壮'], 1, set_strong)
        a.t()
        a.t()
        a.b('确定', confirm_player)
        a.b('返回', a.back)

    def set_gender(value):
        player.data['gender'] = value

    def set_height(value):
        if not value == '普通':
            player.data['tag'].append(value)

    def set_build(value):
        if not value == '普通':
            player.data['tag'].append(value)

    def set_strong(value):
        if not value == '普通':
            player.data['tag'].append(value)

    def confirm_player():
        a.page()
        player.data['money'] = 100
        player.update()
        player.reset()
        a.data['cm'].show_person_profile(player)
        a.t()
        a.t('确定吗？')
        a.t()
        a.b('是', add_person)
        a.t('/')
        a.b('否', a.back)

    def add_person():
        player.save()
        a.data['cm'].add_person(player)
        a.data['db']['player'] = player.data['hash']
        a.data['wm'].add_to_birth_place(player.data['hash'])
        a.page()
        a.t('已设定！', True)
        a.goto(ask_intro)

    player = a.data['cm'].new_null_person()
    player.data['name'] = '亚当'
    player.data['系统称呼'] = '你'
    player.data['自称'] = '我'
    player.data['尊称'] = '你'
    player.data['蔑称'] = '你'
    a.goto(gui_set_name)


def ask_intro():
    a.page()
    a.t('是否欣赏开场剧情？')
    a.t()
    a.b('是', a.goto, intro)
    a.t('/')
    a.b('否', a.goto, short_intro)


def intro():
    a.page()
    for each in a.data['data.others']['opening']['long']:
        last = len(each)
        for every in each:
            a.t(every)
            time.sleep(0.5/last)
            last -= 1
        a.t()
    a.t(' ', True)
    a.goto(short_intro)


def short_intro():
    a.page()
    for each in a.data['data.others']['opening']['short']:
        a.t(each, True)
        a.t()
    a.clear_gui()
    a.goto(loop)


def load_for_load():
    a.data['cm'].load()
    a.data['sm'].load()
    a.data['it'].load()
    a.clear_gui()
    a.goto(loop)


def ui_move_on_map(current_pos):
    def move(target):
        a.data['sm'].move_to(a.data['db']['player'], target)
        a.back()
    a.page()
    a.h('旅行')
    a.t()
    for neighbour in a.data['sm'].get_neighbours(current_pos):
        near_birth_area = False
        for each in a.data['sm'].get_neighbours(neighbour):
            if '出生区域' in each.data and each.data['出生区域'] == True:
                near_birth_area = True
        if near_birth_area or ('出生区域' in neighbour.data and neighbour.data['出生区域']):
            a.b('移动', move, neighbour)
        else:
            a.b('移动')
        if 'type' in neighbour.data:
            name = neighbour.data['type']
        else:
            name = neighbour.data['地形']
        a.t('地形：{}　海拔：{}m　坐标：({}, {})'.format(
            name,
            int(neighbour.data['h']),
            int(neighbour.data['x']),
            int(neighbour.data['y'])
        ))
        a.t()
    a.b('设置目标')
    a.b('向目标移动')
    a.t()
    a.b('返回', a.back)


def loop():
    a.page()
    a.t(random.choice(a.data['data.others']['tips']))
    a.t()
    a.divider()
    a.t()
    m.show_main_panel()
    a.t()
    current_pos_index = a.data['sm'].find_player()
    current_pos = a.data['sm'].get(current_pos_index)

    def enter(target):
        a.data['sm'].move_to(a.data['db']['player'], target)
        a.repeat()

    def back():
        a.data['sm'].leave(a.data['db']['player'])
        a.repeat()
    if isinstance(current_pos, kj.Area):
        a.b('旅行', a.goto, ui_move_on_map, current_pos)
        for each in current_pos.data['child']:
            a.b('去{}'.format(each.data['type']), enter, each)
    elif isinstance(current_pos, kj.Place):
        a.b('离开', back)
        for each in current_pos.data['child']:
            a.b('进入{}'.format(each.data['type']), enter, each)
    elif isinstance(current_pos, kj.Room):
        if current_pos.is_entry():
            a.b('离开', back)
    a.t()
    if isinstance(current_pos, kj.Area):
        if not '出生区域' in current_pos.data:
            a.b('探险', a.goto, m.explore)
    a.t()
    for person in a.data['sm'].find_all_person_in_space(current_pos_index):
        if not person == a.data['cm'].get_player():
            a.b(person.data['name'], a.goto, m.ui_interact_with_person, person)
    a.t()
    # func = a.data['data.地点'][place_type]['功能']
    # for each in func.keys():
    #     if each == '郊区':
    #         a.b('郊区探险', a.goto, m.explore)
    #     elif each == '市场':
    #         a.b('市场', a.goto, m.market)
    #     elif each == '酒馆':
    #         a.b('酒馆', m.bar)
    # a.t()
    a.b('人物管理', a.goto, m.person_list)
    player = a.data['cm'].get_player()
    has_slave = False
    for person_hash in player.data['关系'].keys():
        if '被囚禁者' in player.data['关系'][person_hash]['关系']:
            has_slave = True
            break
    if has_slave:
        a.b('奴隶管理', a.goto, m.ui_slave_list)
    else:
        a.b('奴隶管理', None, disabled=True)
    a.b('装备管理', a.goto, m.equipment, player, True)
    a.b('物品管理', None, disabled=True)
    a.b('百科全书', a.goto, a.data['wiki'].ui_wiki)
    a.b('休息', rest)
    a.t()
    a.b('保存游戏', a.goto, save_game)
    a.b('读取游戏', a.goto, load_game)
    a.b('游戏设置', None, disabled=True)
    a.b('结束游戏', a.exit)
    a.b('结局', None, disabled=True)


def rest():
    a.data['tim'].tick()
    a.page()
    m.rest()
    a.t('经过了半天。', True)
    a.repeat()


def save_game():
    a.data['sm'].save()
    a.data['cm'].save()
    a.data['it'].save()
    a.page()
    a.h('保存游戏')
    a.t()
    a.show_save_to_save()
    a.b('返回', a.back)


def quit_game(a):
    pass


def load_game():
    a.page()
    a.h('读取游戏')
    a.t()
    a.show_save_to_load(load_for_load)
    a.b('返回', a.back)


def game_setting():
    a.page()
    a.h('游戏设置')
    a.t()
    a.b('返回', a.back)


if __name__ == "__main__":  # 程序入口
    try:
        a.init()  # 初始化引擎
        a.data['gm'] = yx.GameManager()  # 初始化游戏
        a.goto(cover)  # 进入游戏封面
    except Exception as e:
        # a.critical('{}'.format(e))
        pass